from deepmultilingualpunctuation import PunctuationModel

# This will force pytorch/transformers (not sure which one is responsible for this)
# to download the model (so it's included in our docker image)
model = PunctuationModel()
