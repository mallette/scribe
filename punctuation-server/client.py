import websockets
import asyncio
import json
import sys


async def send(uri):
    with open("sample.txt", "r") as f:
        message = f.read()
    print(len(message))
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps({"text": message}))
        answer = await websocket.recv()
        print(json.loads(answer))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        uri = sys.argv[1]
    else:
        uri = "ws://localhost:2800"
    asyncio.run(send(uri))
