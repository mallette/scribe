#!/usr/bin/env python3


# Inspired by https://github.com/alphacep/vosk-server/blob/master/websocket/asr_server.py
import json
import os
import asyncio
from dataclasses import dataclass
import websockets
import logging
from deepmultilingualpunctuation import PunctuationModel
import concurrent.futures
import time
import torch

DEBUG_LATENCY = int(os.environ.get('DMP_SERVER_DEBUG_LATENCY', 0))

@dataclass
class PunctuationServerArgs:
    interface: str
    port: int
    loglevel: int
    core_per_punctuation: int
    max_core: int

def process_phrase(model, phrase):
    return model.predict(phrase)

async def punctuate(websocket):
    # Very ugly !
    global model
    loop = asyncio.get_running_loop()
    logging.info(f"Connection from {websocket.remote_address}")
    async for message in websocket:
        try:
            data = json.loads(message)
            assert "text" in data
            clean_text = model.preprocess(data["text"])
            # DMP returns float32 instead of float, and float32 aren't JSON-serializable
            predictions = await loop.run_in_executor(pool, process_phrase, model, clean_text)
            labeled_words = [[x[0], x[1], float(x[2])] for x in predictions]
            # Add artificial latency to help debug latency issue
            # adjust DMP_SERVER_DEBUG_LATENCY environment variable to something greater than 0 to activate this
            # You can then either reduce ping_timeout/ping_interval on the client side to simulate a failed keepalive websocket ping
            # Or simply kill the TCP connection with something like this (as root) :
            # ss -K dport = 2800 (single kill)
            # or, if you are running all of this inside docker :
            # nsenter -t $(docker inspect -f '{{.State.Pid}}' scribe-scribe-1) -n ss -K dport = 2800
            # ss is in iproute2 package, and nsenter in util-linux package
            if DEBUG_LATENCY != 0:
                time.sleep(DEBUG_LATENCY)
            await websocket.send(json.dumps({"predictions": labeled_words}))
        except json.JSONDecodeError:
            logging.error(f"Could not decode '{message}'")


async def start():

    global model
    global pool

    args = PunctuationServerArgs(interface=os.environ.get('DMP_SERVER_INTERFACE', '0.0.0.0'),
                                 port=int(os.environ.get('DMP_SERVER_PORT', 2800)),
                                 loglevel=int(os.environ.get('DMP_SERVER_LOGLEVEL', 30)),
                                 core_per_punctuation=int(os.environ.get('DMP_SERVER_CORE_PER_PUNCTUATION', -1)),
                                 max_core=int(os.environ.get('DMP_SERVER_MAX_CORE', -1)))

    logging.basicConfig(level=args.loglevel)
    model = PunctuationModel()
    # Don't guess core usage, use supplied value instead
    if args.max_core != -1:
        logging.warning(f"Starting DMP with custom max core: {args.max_core}")
        pool = concurrent.futures.ThreadPoolExecutor((args.max_core)) 
    else:
        pool = concurrent.futures.ThreadPoolExecutor((os.cpu_count() or 1)) 
    # Don't let torch guess core usage, use supplied value instead
    if args.core_per_punctuation != -1:
        logging.warning(f"Starting DMP with custom core per punctuation: {args.core_per_punctuation}")
        torch.set_num_threads(args.core_per_punctuation)
    if DEBUG_LATENCY != 0:
        logging.error(f"Starting DMP with debug latency {DEBUG_LATENCY} seconds. THIS IS A BAD IDEA IN PRODUCTION. It will make your punctuation very, very, slow")

    async with websockets.serve(punctuate, args.interface, args.port):
        await asyncio.Future()


if __name__ == '__main__':
    asyncio.run(start())
