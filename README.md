# Scribe

Scribe est un service web développé par les [Ceméa](https://cemea.asso.fr) proposant une interface web de transcription audio basé sur le projet [vosk-server](https://github.com/alphacep/vosk-server).

Ce service est peut être installé sur un serveur disposant des pré-requis suivants :

## Spécifications techniques matérielles du serveur

Ceci est un exemple pour quelques transcriptions par mois, uniquement en français
* Processeur : 2 cœurs (virtuels ou physiques) (plus il y a de coeurs et plus le nombre de transcriptions en parallèle sont possibles)
* RAM : 12 Go (3Go pour la ponctuation et 6 Go par langue)
* Disque dur : 50 Go d’espace alloué minimum, sur un disque dur mécanique ou un disque SSD.

## Spécifications techniques logicielles

Le serveur et sa configuration doivent disposer des éléments suivants :
* Distribution Linux : Debian récente (version 11 ou version 12), ou Ubuntu LTS supportée (22.04, 24.04, 20.04)
* Un accès en SSH, avec les droits sudo, sur la machine de destination
* Un accès SMTP, pour que Scribe puisse envoyer des mails aux utilisateur⋅ices
* Un accès à Internet depuis le serveur, afin de télécharger toutes les dépendances utiles, et de permettre la supervision
* Un nom de domaine (privé ou public) dédié à Scribe, avec un certificat lié pour un accès HTTPS

## Confidentialité des données

Les fichiers audio/vidéo envoyés, ainsi que leur transcription, sont supprimés dès la fin du traitement (réception du deuxième mail). Des statistiques (afin de calibrer le service et la puissance nécessaire) sont stockées de manière purement anonyme, sous la forme suivante :
```
{"total_time": 45.309041, "download_time": 0.006695, "convert_time": 0.377968, "transcribe_time": 44.924378, "audio_duration": 235.78125, "ratio": 0.19216558144466533}
```

# Installation avec Docker

Il est nécessaire d'installer Docker sur le système pour le faire fonctionner et de déposer le contenu du code de Scribe ici présent dans un dossier (ex : '/srv/'scribe')

## Production

Pour lancer Scribe, ainsi qu'un vosk server pour le modèle français et anglais (attention, cela prendra environ 8 go de ram), lancez simplement :

```
cp settings.toml.example settings.toml
docker compose up -d
```

Ensuite, rendez-vous sur `http://localhost:8080`.

Pour modifier la configuration, modifiez le `settings.toml` puis relancez scribe avec `docker compose restart scribe`.

En mode production, scribe tourne derrière gunicorn (pour le détail de la configuration, voir `Dockerfile`)

## Développement

Pour lancer un serveur flask de développement, avec juste le modèle français :
```
docker compose -f docker-compose.yml -f docker-compose.dev.yml up
```

Scribe reste accessible à l'adresse `http://localhost:8080`, et le mode dev de flask (avec l'auto-reload, etc) est fonctionnel.

# Installation manuelle

## Pré-requis

* Un système pouvant faire tourner docker
* ffmpeg installé (voir plus bas pour des précisions)
* Sous certaines debian, avoir installé python-is-python3

## Installation

Rien d'inhabituel :

```
git clone https://gitlab.cemea.org/mallette/scribe
cd scribe
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Dépendances
### vosk-server

Pour cela, rien de plus simple :

```
 docker run -d -p 2700:2700 alphacep/kaldi-fr:latest
 docker run -d -p 2701:2700 alphacep/kaldi-en:latest
```

Il est possible de ne faire tourner qu'un seul modèle, ou plus de 2. Attention cependant, chaque modèle consomme environ 4 GO de RAM, prévoyez la machine en conséquence ! Scribe communiquant avec le vosk-server via des websockets, il est tout à fait possible d'héberger les modèles sur un (ou plusieurs) autres serveurs

### DMP-server

#### Installation des prérequis

```
cd punctuation-server
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

#### Lancement

En veillant à être dans le venv de punctuation-server :

```
DMP_SERVER_INTERFACE=0.0.0.0 DMP_SERVER_PORT=2800 python3 server.py
```

## Configuration

Copiez le fichier `settings.toml.example` en `settings.toml` puis modifiez le à votre guise. Le paramètre `EXCLUDED_EXTRACTORS` permet de refuser les URLs utilisant certains extracteurs yt-dlp. Dans l'exemple, `dailymotion` est exclu car dailymotion bloque les URL venant d'une IP d'hébergeur classique.

Le serveur de ponctuation est configuré pour être aussi rapide que possible, en utilisant l'ensemble des coeurs disponible sur votre ordinateur. Cet usage n'est pas adapté si votre instance de Scribe fait tourner de nombreuses transcriptions en parallèle (car vous avez une instance publique, par exemple). Dans ce cas, nous vous recommandons de passer la variable d'environnement suivante (dans le docker-compose.yml, par exemple) au serveur de ponctuation :
```
DMP_SERVER_CORE_PER_PUNCTUATION=1
```
Cela a pour effet de limiter une demande de ponctuation individuelle à un seul coeur, tout en gardant les autres coeurs disponible pour les autres transcriptions.

### Serveur mail

Si vous ne souhaitez pas utiliser les mails (par exemple, pour un usage local), il suffit de régler `use_mail = false` dans votre `settings.toml`. Dans ce cas, les .zip générés seront exportés vers `instance/{random_token}.zip`.

Il n'est possible de se connecter qu'à un serveur mail supportant SSL/TLS (pas de STARTTLS).

Si votre serveur mail n'utilise pas d'authentification, laissez la variable `user` et `password` vide (`''`).

### FFmpeg

La version de FFmpeg dans certaines distributions (Debian 11, Ubuntu 20.04 par exemple) est trop vieille pour que le téléchargement fonctionne correctement sur certains sites, par exemple chez vimeo (cf [repo yt-dlp](https://github.com/yt-dlp/yt-dlp/issues/1040). Pour cela, il est recommandé de télécharger une version récente de ffmpeg, de préférence les builds statique de yt-dlp disponibles sur [leur repo](https://github.com/yt-dlp/FFmpeg-Builds), et de changer `ffmpeg-location` dans votre `settings.toml`. Exemple pour télécharger `ffmpeg` dans `/usr/local/bin/` :
```
cd /tmp
wget https://github.com/yt-dlp/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz
tar xf ffmpeg-master-latest-linux64-gpl.tar.xz
mv ffmpeg-master-latest-linux64-gpl/bin/ffmpeg /usr/local/bin
chmod +x /usr/local/bin/ffmpeg
```

### Traduction

Il est possible de connecter scribe à [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate) pour permettre la traduction automatique. Pour cela, le plus simple est de lancer LibreTranslate avec docker :
```
docker run -d -p 2699:5000 libretranslate/libretranslate
```
Et de régler `use_translation = true` dans votre `settings.toml`.

## Lancement 

### Mode dev

En mode dev avec flask :
```
export FLASK_APP=scribe
export FLASK_ENV=development
flask run
```
Ensuite, vous pourrez visiter `http://localhost:5000` pour vérifier que tout fonctionne

### Déploiement réel (en production)

`flask run` n'étant pas adapté à un vrai déploiement, il vaut mieux utiliser gunicorn + nginx. Dans ce cas, il faudra installer gunicorn :

```
pip install gunicorn
```
Vous trouverez ensuite un fichier d'exemple pour lancer gunicorn via systemd dans `systemd`, et un fichier exemple de reverse-proxy nginx dans `nginx`.

## Utilisation

* se rendre sur la page du service https://scribe.domaine.ext
* Cliquer pour sélectionner son fichier ou coller l'URL d'une vidéo Youtube / Peertube...
* Indiquer une adresse e-mail (obligatoire)
* Envoyer et patienter (environ le temps de la durée de l'audio ou vidéo)
* Consulter votre messagerie et cliquer pour télécharger le fichier .Zip contenant les fichiers textes transcrits par le Scribe.

## Contenu du projet

### settings.toml

Contient la configuration du site. Copier `settings.toml.example` en `settings.toml` et compléter les champs utile

### nginx & systemd

Contient des exemples pour faire tourner scribe avec gunicorn via systemd + nginx

### scribe/

#### __init__.py

L'application Flask à proprement parler, avec ses routes, est créé dans ce fichier.

#### types.py

Contient les informations de type spécifique à ce projet

#### utils.py

Contient des fonctions utilitaires "générique" pas forcément spécifique à Scribe

#### lib.py

Contient les fonctions spécifique à Scribe (communication avec vosk, traitement des résultats ...)

#### templates/

Contient les modèles de pages affichés et les mails envoyés

#### static/

Contient les fichiers CSS + les images nécessaire.
