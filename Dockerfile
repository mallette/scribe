FROM python:3.10-bullseye

RUN apt update && apt install xz-utils
WORKDIR /scribe
# Copy doesn't copy directory, only files, so we do it ourself
COPY scribe/ /scribe/scribe/
COPY requirements.txt /scribe/
RUN pip install -r requirements.txt
# COPY settings.toml.example /scribe/settings.toml
# install the correct ffmpeg version
RUN cd /tmp && wget https://github.com/yt-dlp/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz && tar xf ffmpeg-master-latest-linux64-gpl.tar.xz && mv ffmpeg-master-latest-linux64-gpl/bin/ffmpeg /usr/bin && chmod +x /usr/bin/ffmpeg && rm -Rf /tmp/ffmpeg*

EXPOSE 8080

CMD ["/usr/local/bin/gunicorn", "scribe:create_app()", "--worker-tmp-dir", "/dev/shm", "--bind", "0.0.0.0:8080", "--access-logfile", "-", "--error-logfile", "-", "--capture-output"]
