from typing import NewType, TypedDict, TypeVar, Callable, Any, List
from dataclasses import dataclass
from enum import Enum
import datetime

MailAddress = NewType('MailAddress', str)
VoskUri = NewType('VoskUri', str)

# Used for decorators
F = TypeVar('F', bound=Callable[..., Any])


class Error(Exception):
    def __init__(self, message) -> None:
        self.message = message

class PunctuaterTimeoutReached(Exception):
    def __init__(self, message) -> None:
        self.message = message

class ScribeOutput(Enum):
    Mail = 1
    Local = 2


class VoskWord(TypedDict):
    # 0-1, vosk confidence
    conf: float
    # in seconds since start of audio
    start: float
    # in seconds since start of audio
    end: float
    # Always a single word (no space inside)
    word: str


class VoskPartial(TypedDict):
    partial: str


class VoskPhrase(TypedDict):
    result: List[VoskWord]
    text: str


@dataclass(init=False)
class Timings:
    start: datetime.datetime
    video_done: datetime.datetime
    convert_done: datetime.datetime
    end: datetime.datetime
    audio_duration: float


DMPWord = NewType('DMPWord', tuple[str, str, float])
