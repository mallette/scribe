from abc import ABCMeta, abstractmethod
from io import BytesIO
from pathlib import Path
import secrets
from dataclasses import dataclass
import smtplib
from email.message import EmailMessage
from typing import Optional

from flask import render_template

from .types import MailAddress


class VirtualSender(metaclass=ABCMeta):
    @abstractmethod
    def set_display_name(self, display_name: str) -> None:
        pass

    @abstractmethod
    def send_start(self) -> None:
        pass

    @abstractmethod
    def send_done(self, archive: BytesIO, total_time: str) -> None:
        pass

    @abstractmethod
    def send_error(self, archive: BytesIO) -> None:
        pass


@dataclass
class LocalSender(VirtualSender):
    path: Path

    def set_display_name(self, display_name: str) -> None:
        pass

    def send_start(self) -> None:
        pass

    def send_done(self, archive: BytesIO, total_time: str) -> None:
        filename = secrets.token_hex(16) + ".zip"
        with open(self.path / filename, "wb") as f:
            f.write(archive.getbuffer())

    def send_error(self, archive: BytesIO) -> None:
        filename = secrets.token_hex(16) + ".zip"
        with open(self.path / filename, "wb") as f:
            f.write(archive.getbuffer())


@dataclass
class MailSender(VirtualSender):
    server: str
    port: int
    user: str
    password: str
    mail_from: MailAddress
    lang: str
    site_url: str
    admin_mail: MailAddress
    send_errors_to_admin: str
    mail_to: MailAddress
    display_name: str
    site_title: str

    def set_display_name(self, display_name: str) -> None:
        self.display_name = display_name

    def send_start(self) -> None:
        title = f"{self.site_title} : Lancement"
        content = render_template("start.txt", lang=self.lang, display_name=self.display_name, site_url=self.site_url)
        self._send(self.mail_to, title, content, None)

    def send_done(self, archive: BytesIO, transcription_time: str) -> None:
        title = f"{self.site_title} : FIN"
        content = render_template("end.txt", lang=self.lang, display_name=self.display_name,
                                  site_url=self.site_url, transcription_time=transcription_time)
        self._send(self.mail_to, title, content, archive)

    def send_error(self, archive: BytesIO) -> None:
        title = f"{self.site_title} : ERREUR"
        content = render_template("error.txt", lang=self.lang, display_name=self.display_name,
                                  admin_mail=self.admin_mail,
                                  site_url=self.site_url)
        self._send(self.mail_to, title, content, archive)
        if self.send_errors_to_admin:
            self._send(self.admin_mail, title, content, archive)

    def _send(self, mail_to: MailAddress, title: str, content: str, archive: Optional[BytesIO]) -> None:
        # Basic content
        message = EmailMessage()
        message.set_content(content)
        # Basic headers
        message["From"] = self.mail_from
        message["To"] = mail_to
        message["Subject"] = title
        # Attachment is a little bit tricky
        if archive is not None:
            message.add_attachment(
                archive.getbuffer(),
                maintype='application',
                subtype='zip',
                filename="transcription.zip")
        # login time (only using SMTPS)
        server = smtplib.SMTP_SSL(host=self.server, port=self.port)
        # Don't authenticate if we don't any information
        # This adds support for authless SMTP server
        if self.user != "" and self.password != "":
            server.login(self.user, self.password)
        # Send time
        server.send_message(message)
        server.quit()
