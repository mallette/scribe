from typing import List
from dataclasses import dataclass
import json

import requests

from .types import VoskPhrase


@dataclass
class LibreTranslater:
    url: str
    source_lang: str
    target_lang: str

    def translate(self, phrases: List[VoskPhrase]) -> List[VoskPhrase]:
        """
        This is a bit tricky : we need to preserve a List[VoskPhrase] (we need start/end for every phrase)
        But LibreTranslate takes only a string. Luckily, it translate each line independently, so we can generate
        a big string with all our phrases concatenated, send it
        And when we get the result, replace the "text" in VoskPhrase with our translated text
        """
        as_str = "\n".join([p["text"] for p in phrases])
        payload = {"source": self.source_lang, "target": self.target_lang, "q": as_str}

        r = requests.post(self.url, data=json.dumps(payload), headers={"Content-Type": "application/json"})
        if r.status_code != 200:
            r.raise_for_status()
        translated = r.json()["translatedText"]
        out = []
        for i, line in enumerate(translated.split("\n")):
            phrase = VoskPhrase({"result": phrases[i]["result"], "text": line})
            out.append(phrase)
        return out
