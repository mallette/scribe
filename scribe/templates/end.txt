La transcription de votre fichier/URL suivant est terminée en {{ transcription_time }}:
* {{ display_name }}

La langue de traduction utilisée est : "{{ lang }}"

Votre transcription est disponible en pièce jointe de ce mail.

*********

Explications

Ce fichier Zip contient plusieurs fichiers :
- phrases.txt : fichier contenant le texte brut complet
- timed_phrases.txt : fichier contenant le texte avec le timing de chaque phrase
- subtitles.txt : fichier spécifique pour sous-titres de vidéos

Merci.

--
Scribe, le transcripteur Audio vers texte
{{ site_url }}

* Scribe est un service libre, éthique, gracieusement mis à disposition par les Ceméa,
association d'éducation populaire et mouvement d'éducation nouvelle,
qui n'exploite pas vos données personnelles.
En savoir plus : {{ site_url }}donnees
* Les Ceméa soutiennent les communs numériques et peuvent vous accompagner pour disposer 
d'autres services libres, éthiques et respectueux des utilisateurs·rices.
https://cemea.asso.fr/contact

