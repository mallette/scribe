import logging
import os

from pathlib import Path
from multiprocessing import Process
from typing import Union

import werkzeug.wrappers.response
from werkzeug.datastructures import FileStorage
from flask import Flask, render_template, request, url_for, current_app, redirect
import toml

from .types import ScribeOutput, Error
from .lib import get_file, get_url, get_mail, get_lang, get_translation_lang, get_punctuation
from .utils import ALLOWED_EXTENSIONS, error_wrapper, get_ip, get_translation_langs
from .senders import MailSender, LocalSender, VirtualSender
from .translater import LibreTranslater
from .punctuater import DMPPunctuater
from .transcribers import FileTranscriber, YoutubeTranscriber, VirtualTranscriber


def render_error(message: str) -> str:
    return render_template("result_bad.html", title=current_app.config['SITE_TITLE'] + " - Erreur",
                           message=message)


def index() -> str:
    # translations are optional, don't sweat it if the service is down for some reason, just ignore 'em
    try:
        if current_app.config['USE_TRANSLATION']:
            translation_langs = get_translation_langs(current_app.config['TRANSLATION']['url_languages'])
        else:
            translation_langs = None
    except Exception:
        translation_langs = None
    return render_template("index.html", title=current_app.config['SITE_TITLE'], models=current_app.config['MODELS'],
                           ALLOWED_EXTENSIONS=ALLOWED_EXTENSIONS, use_mail=current_app.config['USE_MAIL'],
                           use_punctuation=current_app.config['USE_PUNCTUATION'],
                           translation_langs=translation_langs)


def why() -> str:
    return render_template("why.html", title=current_app.config['SITE_TITLE'] + ' - Pourquoi')


def persodata() -> str:
    return render_template("persodata.html", title=current_app.config['SITE_TITLE'] + ' - Vos données')


def cgu() -> str:
    return render_template("cgu.html", title=current_app.config['SITE_TITLE'] + ' - Mentions légales')


def changelog() -> str:
    return render_template("changelog.html", title=current_app.config['SITE_TITLE'] + ' - Évolutions du site')


def errors() -> str:
    return render_template("erreur.html", title=current_app.config['SITE_TITLE'] + ' - Erreurs de Scribe et résolutions')


def start() -> Union[werkzeug.wrappers.response.Response, str]:  # noqa: C901
    if request.method == 'GET':
        return redirect(url_for('index'))

    # Let's get our data
    file = get_file(request)  # Optional (can send URL instead)
    url = get_url(request, current_app.config['EXCLUDED_EXTRACTORS'])  # Optional (can send file instead)
    mail_to = get_mail(request)  # Optional (not available in StorageOutput.Local mode)
    lang = get_lang(request, current_app.config['MODELS'])  # Mandatory
    ip = get_ip(request)
    if current_app.config['USE_TRANSLATION']:
        translation_lang = get_translation_lang(request)
    else:
        translation_lang = None
    if current_app.config['USE_PUNCTUATION']:
        use_punctuation = get_punctuation(request)
    else:
        use_punctuation = None

    # Simple error checking (it's a bit ugly but eh)
    if isinstance(lang, Error):
        return render_error(lang.message)
    if isinstance(file, Error):
        return render_error(file.message)
    if isinstance(url, Error):
        return render_error(url.message)
    if isinstance(mail_to, Error):
        return render_error(mail_to.message)
    if isinstance(translation_lang, Error):
        return render_error(translation_lang.message)
    if isinstance(use_punctuation, Error):
        return render_error(use_punctuation.message)
    if file is None and url is None:
        return render_error("Merci d'envoyer un fichier ou de rentrer une URL")
    if current_app.config['OUTPUT_LOCATION'] == ScribeOutput.Mail and mail_to is None:
        return render_error("Vous n'avez pas rentré de mail")

    vosk_uri = current_app.config['MODELS'][lang.upper()]['uri']
    timings_path = Path(current_app.instance_path) / "timings.json"
    nbcharts_path = Path(current_app.instance_path) / "../scribe/static/nbbar_chart.svg"
    longcharts_path = Path(current_app.instance_path) / "../scribe/static/longbar_chart.svg"
    if current_app.config['OUTPUT_LOCATION'] == ScribeOutput.Mail and request.form.get('log_mail') == "yes":
        logged_mail = mail_to
    else:
        logged_mail = None

    if current_app.config['OUTPUT_LOCATION'] == ScribeOutput.Mail:
        # Mypy cannot infer it
        assert mail_to is not None
        # This is a big one, not much we can do about it
        sender: VirtualSender = MailSender(server=current_app.config['MAIL']['server'],
                                           port=current_app.config['MAIL']['port'],
                                           user=current_app.config['MAIL']['user'],
                                           password=current_app.config['MAIL']['password'],
                                           mail_from=current_app.config['MAIL']['mail_from'],
                                           lang=lang,
                                           site_url=url_for('index', _external=True),
                                           admin_mail=current_app.config['ADMIN_MAIL'],
                                           send_errors_to_admin=current_app.config['SEND_ERRORS_TO_ADMIN'],
                                           mail_to=mail_to,
                                           display_name="",
                                           site_title=current_app.config['SITE_TITLE'])  # a bit ugly

    elif current_app.config['OUTPUT_LOCATION'] == ScribeOutput.Local:
        sender = LocalSender(path=Path(current_app.instance_path))

    if translation_lang is not None:
        translater = LibreTranslater(current_app.config["TRANSLATION"]["url_translation"],
                                     lang.lower(), translation_lang)
    else:
        translater = None

    if use_punctuation:
        punctuater = DMPPunctuater(current_app.config['PUNCTUATION']['uri'])
    else:
        punctuater = None
    # Now let's create our transcriber
    if isinstance(file, FileStorage):
        transcriber: VirtualTranscriber = FileTranscriber(current_app.logger, sender, translater, punctuater, vosk_uri, timings_path, nbcharts_path, longcharts_path, logged_mail, ip, lang, current_app.config['MAX_CONCURRENT_TRANSCRIPTION'], file)
        display_name = file.filename
    elif isinstance(url, str):
        transcriber = YoutubeTranscriber(current_app.logger, sender, translater, punctuater,
                                         vosk_uri, timings_path, nbcharts_path, longcharts_path, logged_mail, ip, lang, current_app.config['MAX_CONCURRENT_TRANSCRIPTION'], url,
                                         current_app.config['FFMPEG_LOCATION'])
        display_name = url

    # And then use it, easy peasy
    p = Process(
        target=error_wrapper(transcriber.start, current_app.logger, [sender.send_error]),
        daemon=True)
    p.start()

    # All done !
    return render_template("result_OK.html", title=current_app.config['SITE_TITLE'] + ' - Résultat',
                           display_name=display_name)


def create_app() -> Flask:
    """
    Flask run (for development) run this automagically, and for development we explicitely tell gunicorn to use this as an
    entrypoint
    """
    app = Flask(__name__)
    # Standard init from toml
    # We don't use directly app.config.from_file because this resolves path according to __init__.py location
    # and not based on PWD() for flask run / gunicorn
    with open("settings.toml", "r") as f:
        app.config.update(toml.load(f))
    app.secret_key = app.config['SECRET']
    # We need it to store timings
    os.makedirs(app.instance_path, exist_ok=True)
    if app.config['USE_MAIL']:
        app.config['OUTPUT_LOCATION'] = ScribeOutput.Mail
    else:
        app.config['OUTPUT_LOCATION'] = ScribeOutput.Local

    gunicorn_logger = logging.getLogger('gunicorn.error')
    # We are running inside gunicorn, hook ourself to gunicorn
    if gunicorn_logger.hasHandlers():
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)
        app.logger.info("Hooking scribe to gunicorn logger")
    # let's customize logging a bit
    if len(app.logger.handlers) == 0:
        raise RuntimeError("Flask should initialize with at least one logger, none found, aborting")
    formatter = logging.Formatter("[{asctime}] {levelname} in {module} - {funcName}:{lineno} : {message}", style="{")
    for x in app.logger.handlers:
        x.setFormatter(formatter)

    app.logger.info("Starting scribe")
    # Let's calculate how many transcriptions are allowed
    app.config['MAX_CONCURRENT_TRANSCRIPTION'] = app.config['TRANSCRIPTION_PER_CORE'] * os.cpu_count()
    app.logger.info(f"CPU cores available : {os.cpu_count()}, max concurrent transcriptions : {app.config['MAX_CONCURRENT_TRANSCRIPTION']}")

    # Now let's add our routes

    app.add_url_rule("/", view_func=index, methods=['GET'])
    # GET is allowed here only to redirect to /
    app.add_url_rule("/start/", view_func=start, methods=['POST', 'GET'])
    app.add_url_rule("/pourquoi/", view_func=why, methods=['GET'])
    app.add_url_rule("/donnees/", view_func=persodata, methods=['GET'])
    app.add_url_rule("/mentions-legales/", view_func=cgu, methods=['GET'])
    app.add_url_rule("/evolutions/", view_func=changelog, methods=['GET'])
    app.add_url_rule("/erreurs/", view_func=errors, methods=['GET'])

    return app
