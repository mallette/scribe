import wave
import json
import datetime
from pathlib import Path
from typing import Union, cast, List
import contextlib

import flask.wrappers
import websockets.client
import validators  # type: ignore
import srt  # type: ignore
from werkzeug.datastructures import FileStorage

from .types import VoskPartial, VoskPhrase, Error, MailAddress
from .utils import allowed_extension, get_extractor


def get_file(request: flask.wrappers.Request) -> Union[FileStorage, Error, None]:
    if 'file' not in request.files:
        return None
    file = request.files['file']
    # If no file is selected, browser submit an empty file with enmpty filename
    if not file.filename:
        return None
    if not allowed_extension(file.filename):
        return Error("Cette extension n'est pas autorisée")
    return file


def get_url(request: flask.wrappers.Request, excluded_extractors: List[str]) -> Union[str, Error, None]:
    url = request.form.get('url')
    if url is None or url == "":
        return None
    if not validators.url(url):
        return Error("Votre URL est invalide")
    extractor = get_extractor(url)
    if extractor is None:
        return Error("""Votre URL pointe vers un site qui n'est pas supporté par Youtube-DL.
                Vous pouvez toujours la télécharger manuellement et l'envoyer comme un fichier.""")
    if extractor in excluded_extractors:
        return Error(
            """Votre URL pointe vers un hébergeur qui bloque les téléchargements automatisés depuis des scripts.
                     Vous pouvez toujours télécharger manuellement votre vidéo et l'envoyer comme un fichier, ou en extraire uniquement le son.""")
    return url


def get_mail(request: flask.wrappers.Request) -> Union[MailAddress, Error, None]:
    email = request.form.get('email')
    if email is None:
        return None
    if not validators.email(email):
        return Error("Votre email est invalide")
    return MailAddress(email)


def get_lang(request: flask.wrappers.Request, models) -> Union[str, Error]:
    lang = request.form.get('lang')
    if lang is None:
        return Error("Vous n'avez pas choisi de langue")
    if lang.upper() not in models:
        return Error("Impossible de trouver de modèle correspondant à votre langue")
    return lang


def get_translation_lang(request: flask.wrappers.Request) -> Union[Error, None, str]:
    if request.form.get('translate_text') != "yes":
        return None
    target = request.form.get('translate_target')
    if target is None:
        return None
    return target


def get_punctuation(request: flask.wrappers.Request) -> Union[Error, None, bool]:
    result = request.form.get('use_punctuation')
    if result is None:
        return None
    else:
        return result == "yes"


async def send_to_vosk(uri: str, audio_path: Path) -> List[Union[VoskPartial, VoskPhrase]]:
    """
    Send a buffer of .wav data to a websocket server
    See alphacep/vosk-server/ for example implementations
    """
    async with websockets.client.connect(uri) as websocket:
        with contextlib.closing(wave.open(str(audio_path), "rb")) as wf:
            await websocket.send('{ "config" : { "sample_rate" : %d } }' % (wf.getframerate()))

            results = []
            buffer_size = int(wf.getframerate() * 0.2)  # 0.2 seconds of audio
            while True:
                data = wf.readframes(buffer_size)

                if len(data) == 0:
                    break

                await websocket.send(data)
                results.append(json.loads(await websocket.recv()))

            await websocket.send('{"eof" : 1}')
            # Don't forget the last result which can contain interesting data
            results.append(json.loads(await websocket.recv()))
            return results


def filter_vosk_success(results: List[Union[VoskPartial, VoskPhrase]]) -> List[VoskPhrase]:
    """
    Filter the partial result out.
    We could do it directly in send_to_vosk, but we might need partial results, one day
    """
    return [cast(VoskPhrase, x) for x in results if "result" in x]


def to_phrases(results: List[VoskPhrase], with_timing: bool = False) -> str:
    """
    Return all phrases from audio
    Optionally, add timings (in the form [HH'MM'SS])
    """

    # Very simple, one phrase = one row
    if not with_timing:
        out = [x["text"] for x in results]
    # Let's prepend every phrase with [HH'MM'SS]
    else:
        out = []
        for x in results:
            # Either i'm blind, or there is no function in stdlib to do that ?!
            total_seconds = int(x["result"][0]["start"])
            hours = total_seconds // 3600
            minutes = (total_seconds % 3600) // 60
            seconds = total_seconds % 60
            elapsed = datetime.time(hour=hours, minute=minutes, second=seconds).strftime("%H'%M'%S")

            out.append(f"[{elapsed}] {x['text']}")
    return "\n".join(out)


def to_subtitles(results: List[VoskPhrase]) -> str:
    """ Return a subtitle string, with one subtitle = one phrase """
    subs = []
    # Vosk return an array of phrases (what it consider as a phrase - i.e when someone stop speaking, it's the end of
    # the phrase
    """
        {'result': [  # An array of words, like this
            {'conf': 0.75,  # Confidence in the result
            'start': 110.01,  # when the word starts in the audio (seconds elapsed since beginning)
            'end': 110.06,  # when the word ends in the audio (seconds elapsed since beginning)
            'word': 'Hello'},  # the word it recognized
            {'conf': 0.715, 'start': 110.10, 'end': 110.16, 'word': 'World'}  # Another word
        ],
        'text': 'Hello World'}  # the complete phrase
    """

    for phrase in results:
        # We want to limit subtitles to 78 character (without cutting in the middle of a word)
        text = ""
        start = datetime.timedelta(seconds=phrase["result"][0]['start'])
        end = datetime.timedelta(seconds=phrase["result"][0]['end'])
        words: List[str] = []
        for word in phrase["result"]:
            # Let's create a subtitle
            if len(text) + len(word["word"]) > 78:
                # Don't put extra space, it's ugly
                s = srt.Subtitle(None, content=" ".join(words), start=start, end=end)
                subs.append(s)
                # Don't forget to update start time !
                start = datetime.timedelta(seconds=word['start'])
                words = [word['word']]
            else:
                words.append(word['word'])
                end = datetime.timedelta(seconds=word['end'])
        # Add the final subtitle
        s = srt.Subtitle(None, content=" ".join(words), start=start, end=end)
        subs.append(s)
    return srt.compose(subs)
