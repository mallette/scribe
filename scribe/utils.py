from pathlib import Path
from typing import Mapping, Callable, List, Optional, Tuple
from io import BytesIO
import traceback
from zipfile import ZipFile
import os
import yt_dlp  # type: ignore
import ffmpeg  # type: ignore
import flask.wrappers

from glob import glob
from logging import Logger
import requests

import pygal # type: ignore

from .types import F

ALLOWED_EXTENSIONS = set(
    ['wav', 'mp3', 'ogg', 'ac3', 'mka', 'm4a', 'flac',
     'wma', 'opus', 'mp4', 'webm', 'avi', 'mkv', 'm4v',
     'mov', 'ogv', 'wmv', 'mpg', 'mpeg', 'flv']
)


def allowed_extension(filename: str) -> bool:
    extension = Path(filename).suffix.lower().replace('.', '')
    return extension in ALLOWED_EXTENSIONS


def get_extractor(url: str) -> Optional[str]:
    extractors = yt_dlp.extractor.gen_extractors()
    for e in extractors:
        if e.suitable(url):
            return e.IE_NAME
    return None


def get_ip(request: flask.wrappers.Request) -> str:
    if "HTTP_X_FORWARDED_FOR" in request.environ:
        return request.environ['HTTP_X_FORWARDED_FOR']
    if "REMOTE_ADDR" in request.environ:
        return request.environ['REMOTE_ADDR']
    if request.remote_addr:
        return request.remote_addr
    raise RuntimeError("Could not find any IP, please check your proxy settings to setup proper forwarding")


def get_translation_langs(url: str) -> List[Tuple[str, str]]:
    r = requests.get(url)
    if r.status_code != 200:
        r.raise_for_status()
    return r.json()


def create_zip(files: Mapping[str, str]) -> BytesIO:
    out = BytesIO()
    with ZipFile(out, 'w') as f:
        for name, content in files.items():
            f.writestr(name, content)
    return out


def error_wrapper(f: Callable, logger: Logger, on_error: List[Callable]) -> Callable[[F], F]:
    """
    Wraps a function for error handling :
    * Get all relevant information (stacktrace, stderr, stdout ...)
    * Create a zip with all this
    * Call on_error with this zip file
    * re-raise the exception (so it appears in logs)
    * For some exception (e.g FFMPEG), log stderr/stdout because they don't appear in original stack
    """
    def wrapper(*args, **kwds):
        try:
            f(*args, **kwds)
        except Exception as e:
            files = {"stack.txt": traceback.format_exc()}
            # ffmpeg gives no useful information in exception, we need to add stderr/stdout
            if isinstance(e, ffmpeg.Error):
                files["stderr.txt"] = e.stderr
                files["stdout.txt"] = e.stdout
                logger.error(e.stderr.decode("utf-8"))
                logger.error(e.stdout.decode("utf-8"))
            error_zip = create_zip(files)
            for c in on_error:
                c(error_zip)
            raise e
    return wrapper

def current_transcription_count() -> int:
    """ Return how many concurrent transcriptions are running"""
    return len(glob("/tmp/*/vosk-running"))