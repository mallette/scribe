from typing import List
from dataclasses import dataclass
import websockets.client
import json

from .types import VoskPhrase, VoskWord, DMPWord, PunctuaterTimeoutReached


@dataclass
class DMPPunctuater:
    uri: str

    def punctuate_phrase(self, phrase: VoskPhrase, prediction: List[DMPWord]) -> VoskPhrase:
        """
        Update a vosk phrase with a prediction result array from deepmultilingualpunctuation
        e.g punctuation is inserted in each result[i]['word'] ; 'Berkeley becomes 'Berkeley,' and so on
        """
        words = []
        capnext = True
        if len(phrase["result"]) != len(prediction):
            raise RuntimeError("len(phrase['result']) != len(prediction)")
        for old_word, (word, label, _) in zip(phrase["result"], prediction):
            if capnext:
                word = word.capitalize()
                capnext = False
            if label in ".,?-:":
                word += label
            if label == ".":
                capnext = True
            v_word: VoskWord = {"conf": old_word["conf"],
                                "start": old_word["start"],
                                "end": old_word["end"],
                                "word": word}
            words.append(v_word)
        final_text = " ".join([x["word"] for x in words])
        new_phrase: VoskPhrase = {"result": words, "text": final_text}
        return new_phrase

    async def punctuate(self, vosk_phrases: List[VoskPhrase]) -> List[VoskPhrase]:
        """
        Sends the phrases to deepmultilingual server to punctuate them
        Punctuation is inserted in each result[i]['word'] - see to_subtitles for more information about the structure
        """
        out = []
        for phrase in vosk_phrases:
            # websocket are relatively cheap, so we can make one connection per phrase
            # It's not very optimised, but ... this project use three differents ML models, are we really picky
            # about a little bit of unefficient code ? :P
            retry_count = 0
            MAX_RETRY_COUNT = 10
            async for websocket in websockets.client.connect(self.uri):
                # Sometimes, the connection between scribe and DMP can timeout, or the TCP handler get dropped (and probably other errors)
                # So if this happens, we'll first retry contacting DMP (maybe it's just a bad apple)
                # Eventually, when we reach MAX_RETRY_COUNT, we give up and pass the exception higher in the callstack
                try:
                    await websocket.send(json.dumps({"text": phrase["text"]}))
                    dmp_result = await websocket.recv()
                    predictions = json.loads(dmp_result)["predictions"]
                    out.append(self.punctuate_phrase(phrase, predictions))
                    # We use a for loop to recreate a connection in case the connection fail, so we need to break out of the loop if everything was good
                    break
                except:
                    retry_count += 1
                    if retry_count > MAX_RETRY_COUNT:
                        raise PunctuaterTimeoutReached(f"Couldn't contact DMP server {self.uri} after trying {retry_count} times")
                    else:
                        continue

        return out
