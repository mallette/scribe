import wave
import contextlib
import tempfile
import datetime
import secrets
import json
from pathlib import Path
from io import BytesIO
import asyncio
from typing import Optional, List
import traceback
import logging
import re
import time
import pygal # type: ignore

from abc import ABCMeta, abstractmethod


from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
import yt_dlp  # type: ignore
import ffmpeg  # type: ignore

from .types import VoskUri, Timings, MailAddress, VoskPhrase

from .utils import create_zip, current_transcription_count
from .senders import VirtualSender
from .translater import LibreTranslater
from .punctuater import DMPPunctuater
from .lib import filter_vosk_success, to_subtitles, to_phrases, send_to_vosk


class FileNameCollectorPP(yt_dlp.postprocessor.common.PostProcessor):
    """
    Taken from https://gist.github.com/jmilldotdev/b107f858729064daa940057fc9b14e89
    Not sure about licensing ...
    """

    def __init__(self):
        super(FileNameCollectorPP, self).__init__(None)
        self.filenames = []

    def run(self, information):
        self.filenames.append(information["filepath"])
        return [], information


class VirtualTranscriber(metaclass=ABCMeta):
    sender: VirtualSender
    translater: Optional[LibreTranslater]
    punctuater: Optional[DMPPunctuater]
    vosk_uri: VoskUri
    timings: Timings
    timings_path: Path
    nbcharts_path: Path
    longcharts_path: Path
    mail: Optional[MailAddress]
    ip: str
    lang: str
    max_concurrent_transcription: float

    def __init__(self, logger: logging.Logger, sender: VirtualSender, translater: Optional[LibreTranslater], punctuater: Optional[DMPPunctuater], vosk_uri: VoskUri, timings_path: Path, nbcharts_path: Path, longcharts_path: Path, mail: Optional[MailAddress], ip: str, lang: str, max_concurrent_transcription: float) -> None:
        self.logger = logger
        self.sender = sender
        self.translater = translater
        self.punctuater = punctuater
        self.vosk_uri = vosk_uri
        self.timings = Timings()
        self.timings_path = timings_path
        self.nbcharts_path = nbcharts_path
        self.longcharts_path = longcharts_path
        self.mail = mail
        self.ip = ip
        self.lang = lang
        self.max_concurrent_transcription = max_concurrent_transcription

    def convert(self, filename: Path, temp_folder: Path) -> Path:
        """
        Using stdin/stdout is a pain, so we use regular files for ffmpeg (for my own sanity)
        """
        stream = ffmpeg.input(filename)
        out_filename = secrets.token_hex(16) + ".wav"
        stream = ffmpeg.output(stream, str(temp_folder / out_filename), f='wav', ac=1, ar='16000')
        ffmpeg.run(stream, quiet=True)
        return temp_folder / out_filename

    def generate_archive(self, vosk_phrases: List[VoskPhrase]) -> BytesIO:
        files = {}
        files["phrases.txt"] = to_phrases(vosk_phrases)
        files["timed_phrases.txt"] = to_phrases(vosk_phrases, with_timing=True)
        files["subtitles.srt"] = to_subtitles(vosk_phrases)
        if self.translater is not None:
            # We don't want to fail transcription if translation failed, let's log the error
            # and move on
            try:
                translated = self.translater.translate(vosk_phrases)
                files["phrases_translated.txt"] = to_phrases(translated)
                files["timed_phrases_translated.txt"] = to_phrases(translated, with_timing=True)
                files["subtitles_translated.srt"] = to_subtitles(translated)
            except Exception:
                self.logger.error("Translation error :")
                self.logger.error(traceback.format_exc())
        return create_zip(files)

    def transcribe_audio(self, audio: Path, vosk_uri: VoskUri) -> BytesIO:
        # Let's send data to vosk
        success = filter_vosk_success(asyncio.run(send_to_vosk(vosk_uri, audio)))
        if self.punctuater is not None:
            # We don't want to fail transcription if only punctuation failed, let's log the error
            # and move on
            try:
                success = asyncio.run(self.punctuater.punctuate(success))
            except Exception:
                self.logger.error("Punctuation error :")
                self.logger.error(traceback.format_exc())
        # Let's process results into something usable
        archive = self.generate_archive(success)
        return archive

    def save_timings(self) -> None:
        """
        Don't use directly the self.timings object (which isn't serializable because datetime.datetime anyway
        Let's do some simple math and store the results (so the result is directly usable)
        """
        total_time = (self.timings.end - self.timings.start).total_seconds()
        out = {
            "ip": self.ip,
            "mail": self.mail,
            "total_time": total_time,
            "start_timestamp": self.timings.start.timestamp(),
            "start_timestr": str(self.timings.start),
            "download_time": (self.timings.video_done - self.timings.start).total_seconds(),
            "convert_time": (self.timings.convert_done - self.timings.video_done).total_seconds(),
            "transcribe_time": (self.timings.end - self.timings.convert_done).total_seconds(),
            "audio_duration": self.timings.audio_duration,
            "lang": self.lang
        }
        # Interesting to compare this between differents CPU (and with GPU-acceleration ?)
        out["ratio"] = total_time / self.timings.audio_duration
        with open(self.timings_path, 'a') as f:
            f.write(json.dumps(out) + "\n")
    
    def update_charts(self):
        mydata = {} # liste des données
        listtranscriptions = []
        # Read lines from timings.json
        myfile = open(self.timings_path, 'r')
        lines = myfile.readlines()
        for line in lines:
            line = line.split("\n")[0]
            mydictline = json.loads(line)
            listtranscriptions.append(mydictline)
        nbtranscriptions = {}
        domains = {}

        # On génère un tableau vide à l'arrache avec toutes les dates existantes et des valeurs nulles
        for j in listtranscriptions:
            mytime = j["start_timestr"]
            searched_string = re.search(r'\d{4}-\d{2}', mytime)
            datestr = str(searched_string.group())
            mydata[datestr] = {}
            mydata[datestr]['nbtransc'] = 0
            mydata[datestr]['longtransc'] = 0
        # On récupère les données cumulées pour chaque mois
        for l in listtranscriptions:
            mytime = l["start_timestr"]
            mylength = l["audio_duration"]
            searched_string = re.search(r'\d{4}-\d{2}', mytime)
            datestr = str(searched_string.group())
            mydata[datestr]['nbtransc'] +=  1
            mydata[datestr]['longtransc'] += mylength

        total = 0       
        barnbtrans = []
        barlongtrans = []
        xbartrans = []

        for datetrans in mydata.keys():
            barnbtrans.append(mydata[datetrans]['nbtransc']/30)
            barlongtrans.append(mydata[datetrans]['longtransc']/3600/30)
            # pdb.set_trace()
            minidate = str(datetrans[5:7])+"/"+str(datetrans[2:4])
            xbartrans.append(minidate)
        
        # Create Bar NB graph
        bar_chart = pygal.Bar(fill=True, height=300, style = pygal.style.CleanStyle)
        bar_chart.title = "Nombre moyen de transcriptions par jour"
        bar_chart.x_labels = map(str, xbartrans[-17:])
        bar_chart.add('',barnbtrans[-17:])
        bar_chart.render_to_file(self.nbcharts_path)

        # Create Bar NB graph    
        longbar_chart = pygal.Bar(fill=True, height=300,  style = pygal.style.DefaultStyle)
        longbar_chart.title = "Durée moyennes cumulées de transcriptions en heures par jour"
        longbar_chart.x_labels = map(str, xbartrans[-17:])
        longbar_chart.add('',barlongtrans[-17:])
        longbar_chart.render_to_file(self.longcharts_path)


    def start(self) -> None:
        self.timings.start = datetime.datetime.now()
        self.sender.send_start()
        with tempfile.TemporaryDirectory() as temp_folder:
            filename = self.save(Path(temp_folder))
            self.timings.video_done = datetime.datetime.now()

            audio = self.convert(filename, Path(temp_folder))
            self.timings.convert_done = datetime.datetime.now()

            # Since we use mono WAV file, it's very easy to get duration
            with contextlib.closing(wave.open(str(audio), "rb")) as f:
                self.timings.audio_duration = f.getnframes() / float(f.getframerate())
            # Let's check if we should start the transcription, or wait a bit
            cpu_busy = True
            wait_time = 0
            while cpu_busy:
                tr_count = current_transcription_count()
                if tr_count < self.max_concurrent_transcription:
                    cpu_busy = False
                else:
                    wait_time += 60
                    self.logger.info(f"Too many concurrent transcription ({tr_count}) - max allowed ({self.max_concurrent_transcription}) sleeping for 60s. Total wait time : {wait_time}")
                    time.sleep(60)

            if wait_time > 0:
                self.logger.info(f"Wait ended for transcription, after waiting {wait_time} seconds.")        

            # Allows us to distinguish really running transcription (i.e sending data to VOSK)
            with open(Path(temp_folder) / "vosk-running", "w") as f:
                f.write("running")
            archive = self.transcribe_audio(audio, self.vosk_uri)
            self.timings.end = datetime.datetime.now()
            total_seconds = (self.timings.end - self.timings.start).seconds
            hours = total_seconds // 3600
            minutes = (total_seconds % 3600) // 60
            seconds = total_seconds % 60
            elapsed = datetime.time(hour=hours, minute=minutes, second=seconds).strftime("%H'%M'%S")

            self.sender.send_done(archive, elapsed)
            self.save_timings()
            self.update_charts()

    @ abstractmethod
    def save(self, temp_folder: Path) -> Path:
        pass


class FileTranscriber(VirtualTranscriber):
    file: FileStorage

    def __init__(self, logger: logging.Logger, sender: VirtualSender, translater: Optional[LibreTranslater],
                 punctuater: Optional[DMPPunctuater], vosk_uri: VoskUri, timings_path: Path, nbcharts_path: Path, longcharts_path: Path, 
                 mail: Optional[MailAddress], ip: str, lang: str, max_concurrent_transcription: float, file: FileStorage) -> None:

        super().__init__(logger, sender, translater, punctuater, vosk_uri, timings_path, nbcharts_path, longcharts_path, mail, ip, lang, max_concurrent_transcription)

        self.file = file
        # MyPy cannot infer this
        assert self.file.filename is not None
        self.sender.set_display_name(self.file.filename)

    def save(self, temp_folder: Path) -> Path:
        # MyPy cannot infer this
        assert self.file.filename is not None
        filename = Path(temp_folder) / secure_filename(self.file.filename)
        self.file.save(filename)
        return filename


class YoutubeTranscriber(VirtualTranscriber):
    url: str

    def __init__(self, logger: logging.Logger, sender: VirtualSender, translater: Optional[LibreTranslater],
                 punctuater: Optional[DMPPunctuater], vosk_uri: VoskUri, timings_path: Path, nbcharts_path: Path, longcharts_path: Path,
                 mail: Optional[MailAddress], ip: str, lang: str, max_concurrent_transcription: float, url: str, ffmpeg_location: str) -> None:
        super().__init__(logger, sender, translater, punctuater, vosk_uri, timings_path, nbcharts_path, longcharts_path, mail, ip, lang, max_concurrent_transcription)
        self.url = url
        self.sender.set_display_name(self.url)
        self.ffmpeg_location = ffmpeg_location

    def save(self, temp_folder: Path) -> Path:
        filename_stub = temp_folder / secrets.token_hex(16)
        # Youtube-dl forces us to do some fuckery - without an extension, he's not happy, so let's give him one
        template = f"{filename_stub}.%(ext)s"
        options = {'format': 'bestaudio/best', 'outtmpl': template,
                   'quiet': True, 'ffmpeg-location': self.ffmpeg_location}
        with yt_dlp.YoutubeDL(options) as ydl:
            filename_collector = FileNameCollectorPP()
            ydl.add_post_processor(filename_collector)
            ydl.download([self.url])
            return Path(filename_collector.filenames[0])
