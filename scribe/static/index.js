function onTranslateTextChange() {
    if ($('input[name="translate_text"]:checked').val() === "yes") {
        $('#translateTextAdvanced').removeClass('is-hidden');
    } else {
        $('#translateTextAdvanced').addClass('is-hidden');
    }
}

$('input[name="translate_text"]').change(onTranslateTextChange);
